Download MD distribution

wget -N https://infinitekind.com/previewdl/current/Moneydance_linux_amd64.tar.gz

Extract to get/create directory Moneydance/lib/

tar xvf Moneydance_linux_amd64.tar.gz
ls -l Moneydance/lib/
total 146472
-rw-rw-r--. 1 xxx xxx     56290 Aug 31 13:10 activation.jar
-rw-rw-r--. 1 xxx xxx   1928009 Aug 31 13:10 antlr-3.2.jar
-rw-rw-r--. 1 xxx xxx      1261 Aug 31 13:10 appsrc.jar
-rw-rw-r--. 1 xxx xxx   1876535 Aug 31 13:10 bcprov-jdk16-146.jar
...

Download md-rest from https://bitbucket.org/hleofxquotesteam/md-web/downloads
wget -N https://bitbucket.org/hleofxquotesteam/md-web/downloads/md-rest-0.0.1-SNAPSHOT.zip

unzip md-rest-0.0.1-SNAPSHOT.zip 
Archive:  md-rest-0.0.1-SNAPSHOT.zip
   creating: md-rest-0.0.1-SNAPSHOT/
  inflating: md-rest-0.0.1-SNAPSHOT/md-rest-0.0.1-SNAPSHOT-exec.jar  

Start tool, make sure to specify location to an MD file

java -jar md-rest-0.0.1-SNAPSHOT/md-rest-0.0.1-SNAPSHOT-exec.jar --file=src/main/resources/My_Wealth.moneydance

In another window
  * Access REST URL's

wget http://localhost:8080/accounts
--2020-11-17 13:58:51--  http://localhost:8080/accounts
Resolving localhost (localhost)... 127.0.0.1
Connecting to localhost (localhost)|127.0.0.1|:8080... connected.
HTTP request sent, awaiting response... 200 
Length: unspecified [application/json]
Saving to: ‘accounts’

    [ <=>                                                                                                                                                                      ] 13,660      --.-K/s   in 0s      

2020-11-17 13:58:51 (201 MB/s) - ‘accounts’ saved [13660]


