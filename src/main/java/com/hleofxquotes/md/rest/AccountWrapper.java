package com.hleofxquotes.md.rest;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.infinitekind.moneydance.model.Account;
import com.infinitekind.moneydance.model.Account.AccountType;

public class AccountWrapper implements IAccountWrapper {
    @JsonIgnore
    private final Account account;

    public AccountWrapper(Account account) {
        super();
        this.account = account;
    }

    public Account getAccount() {
        return account;
    }

    @Override
    public String getAccountName() {
        return account.getAccountName();
    }

    @Override
    public String getAccountDescription() {
        return account.getAccountDescription();
    }

    @Override
    public String getUuid() {
        return account.getUUID();
    }

    @Override
    public AccountType getAccountType() {
        return account.getAccountType();
    }
}
