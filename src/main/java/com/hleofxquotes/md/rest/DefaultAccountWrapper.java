package com.hleofxquotes.md.rest;

import com.infinitekind.moneydance.model.Account.AccountType;

public class DefaultAccountWrapper implements IAccountWrapper {
    private String accountName;

    private String accountDescription;

    private String uuid;

    private AccountType accountType;

    @Override
    public String getAccountName() {
        return accountName;
    }

    @Override
    public String getAccountDescription() {
        return accountDescription;
    }

    @Override
    public String getUuid() {
        return uuid;
    }

    @Override
    public AccountType getAccountType() {
        return accountType;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public void setAccountDescription(String accountDescription) {
        this.accountDescription = accountDescription;
    }

    public void setAccountType(AccountType accountType) {
        this.accountType = accountType;
    }

}
