package com.hleofxquotes.md.rest;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.ApplicationArguments;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.hleofxquotes.md.io.AccountBookWrapperUtils;
import com.infinitekind.moneydance.model.Account;
import com.infinitekind.moneydance.model.AccountIterator;
import com.infinitekind.moneydance.model.MoneydanceSyncableItem;
import com.moneydance.apps.md.controller.AccountBookWrapper;

@Component
public class DefaultMdModel implements MdModel {
    private static final Logger LOGGER = LogManager.getLogger(DefaultMdModel.class);

    private final AccountBookWrapper accountBookWrapper;

    public DefaultMdModel(ApplicationArguments args) throws IOException {
        super();

        String fileName = MdRestApplication.argsGetFileName(args);
        String password = MdRestApplication.argsGetPassword(args);

        if (StringUtils.isEmpty(fileName)) {
            try {
                Path mnyFile = MdRestApplication.findMdFile(new File(".").toPath(), ".moneydance");
                if (mnyFile != null) {
                    fileName = mnyFile.toFile().getAbsolutePath();
                }
            } catch (IOException e) {
                LOGGER.warn(e.getMessage());
            }
        }
        LOGGER.info("ARG, fileName={}", fileName);

        if (StringUtils.isEmpty(fileName)) {
            throw new IOException("Please provide a requirement argument: --file=path/to/My_Wealth.moneydance");
        }

        accountBookWrapper = AccountBookWrapperUtils.open(fileName, password);
    }

    @Override
    public List<AccountWrapper> getAccounts() {
        List<AccountWrapper> accounts = new ArrayList<AccountWrapper>();
        AccountIterator iterator = new AccountIterator(accountBookWrapper.getBook());
        while (iterator.hasNext()) {
            Account account = iterator.next();
            accounts.add(new AccountWrapper(account));
        }

        return accounts;
    }

    @Override
    public AccountWrapper getAccount(String uuid) throws IOException {
        Account account = accountBookWrapper.getBook().getAccountByUUID(uuid);
        if (account == null) {
            throw new IOException("Cannot find account with uuid=" + uuid);
        }

        return new AccountWrapper(account);
    }
    
    public void getRecord(String uuid) {
        MoneydanceSyncableItem item = accountBookWrapper.getBook().getItemForID(uuid);
    }
}
