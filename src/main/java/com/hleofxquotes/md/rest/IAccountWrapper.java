package com.hleofxquotes.md.rest;

import com.infinitekind.moneydance.model.Account.AccountType;

public interface IAccountWrapper {

    String getAccountName();

    String getAccountDescription();

    String getUuid();

    AccountType getAccountType();

}