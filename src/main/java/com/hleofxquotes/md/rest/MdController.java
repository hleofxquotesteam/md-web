package com.hleofxquotes.md.rest;

import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.infinitekind.moneydance.model.Account;
import com.infinitekind.tiksync.SyncRecord;

@RestController
public class MdController {
    private static final Logger LOGGER = LogManager.getLogger(MdController.class);

    @Autowired
    private MdModel mdModel;

    @GetMapping("/accounts")
    public List<AccountWrapper> getAccounts() {
        return mdModel.getAccounts();
    }

    @GetMapping("/account/{uuid}")
    public SyncRecord getAccount(@PathVariable String uuid) throws IOException {
        AccountWrapper wrapper = mdModel.getAccount(uuid);
        Account account = wrapper.getAccount();
        SyncRecord syncRecord = account.getSyncInfo();
        return syncRecord;
    }
}
