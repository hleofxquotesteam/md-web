package com.hleofxquotes.md.rest;

import java.io.IOException;
import java.util.List;

public interface MdModel {

    List<AccountWrapper> getAccounts();

    AccountWrapper getAccount(String uuid) throws IOException;

}
