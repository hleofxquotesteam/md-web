package com.hleofxquotes.md.rest;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@ComponentScan("com.hungle.money.component,com.hungle.money.controller")
public class MdRestApplication {
    private static final Logger LOGGER = LogManager.getLogger(MdRestApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(MdRestApplication.class, args);
    }

    public static final Path findMdFile(Path targetDir, String suffix) throws IOException {
        return Files.list(targetDir).filter((p) -> {
            LOGGER.info("p={}", p);
            if (Files.isDirectory(p)) {
                return p.getFileName().toString().endsWith(suffix);
            } else {
                return false;
            }
        }).findFirst().orElse(null);
    }

    public static final String argsGetPassword(ApplicationArguments args) {
        String password = null;
        if (args.containsOption("password")) {
            List<String> values = args.getOptionValues("password");
            if (values.size() > 0) {
                password = values.get(0);
            }
        }
        return password;
    }

    public static final String argsGetFileName(ApplicationArguments args) {
        String fileName = null;
        if (args.containsOption("file")) {
            List<String> values = args.getOptionValues("file");
            if (values.size() > 0) {
                fileName = values.get(0);
            }
        }
        return fileName;
    }

    private static final void argsSetFileName(String absolutePath, ApplicationArguments args) {
        args.getSourceArgs();
    }
}
