package com.hleofxquotes.md.rest;

import java.net.URISyntaxException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.infinitekind.tiksync.SyncRecord;

@SpringBootTest(args = "--file=src/main/resources/My_Wealth.moneydance", webEnvironment = WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
public class MdRestApplicationTest {
    private static final Logger LOGGER = LogManager.getLogger(MdRestApplicationTest.class);

    @Autowired
    private MdController mdController;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void testContextLoads() {
        Assert.assertNotNull(mdController);
    }

    @Test
    public void getAccounts() throws URISyntaxException {
        ResponseEntity<DefaultAccountWrapper[]> response = restTemplate.getForEntity("/accounts",
                DefaultAccountWrapper[].class);
        IAccountWrapper[] accounts = response.getBody();
        Assert.assertNotNull(accounts);
        Assert.assertTrue(accounts.length > 0);

        for (IAccountWrapper account : accounts) {
            String uuid = account.getUuid();
            LOGGER.info("findAccount, uuid={}", uuid);

            SyncRecord record = restTemplate.getForObject("/account/{uuid}", SyncRecord.class, uuid);
            Assert.assertNotNull(record);
            for (String key : record.keySet()) {
                String value = record.get(key);
                LOGGER.info("  key={}, value={}", key, value);
            }
            /*-
            2020-12-23 21:11:27,980 [INFO  ] MdRestApplicationTest.getAccounts(MdRestApplicationTest.java:53) –   key=name, value=Travel Reimbursement
            2020-12-23 21:11:27,980 [INFO  ] MdRestApplicationTest.getAccounts(MdRestApplicationTest.java:53) –   key=tax_category, value=257
            2020-12-23 21:11:27,980 [INFO  ] MdRestApplicationTest.getAccounts(MdRestApplicationTest.java:53) –   key=old_id, value=96
            2020-12-23 21:11:27,980 [INFO  ] MdRestApplicationTest.getAccounts(MdRestApplicationTest.java:53) –   key=pid, value=92
            2020-12-23 21:11:27,980 [INFO  ] MdRestApplicationTest.getAccounts(MdRestApplicationTest.java:53) –   key=obj_type, value=acct
            2020-12-23 21:11:27,980 [INFO  ] MdRestApplicationTest.getAccounts(MdRestApplicationTest.java:53) –   key=currid, value=9fa60de6-4ed2-4a10-9fd8-37d54a14926a
            2020-12-23 21:11:27,980 [INFO  ] MdRestApplicationTest.getAccounts(MdRestApplicationTest.java:53) –   key=id, value=28f6f93f-02aa-44c6-bcb7-9210562eb40d
            2020-12-23 21:11:27,980 [INFO  ] MdRestApplicationTest.getAccounts(MdRestApplicationTest.java:53) –   key=type, value=i
            2020-12-23 21:11:27,981 [INFO  ] MdRestApplicationTest.getAccounts(MdRestApplicationTest.java:53) –   key=curr, value=5471547
            2020-12-23 21:11:27,981 [INFO  ] MdRestApplicationTest.getAccounts(MdRestApplicationTest.java:53) –   key=parentid, value=eb715a1a-d35b-4182-87e0-d94b37508519
            2020-12-23 21:11:27,981 [INFO  ] MdRestApplicationTest.getAccounts(MdRestApplicationTest.java:53) –   key=sbal, value=0
            2020-12-23 21:11:27,981 [INFO  ] MdRestApplicationTest.getAccounts(MdRestApplicationTest.java:53) –   key=ts, value=1526193103716
             */
            Assert.assertEquals("acct", record.get("obj_type"));
            Assert.assertEquals(uuid, record.get("id"));

        }
    }
}
